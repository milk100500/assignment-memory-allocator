#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static void* map_pages(void const* addr, size_t length, int additional_flags) {
        return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | additional_flags , -1, 0 );
}

struct block_header* find_last_block(struct block_header* h) {
        struct block_header *ret = h;
        while (ret->next) {
                ret = ret->next;
        }
        return ret;
}

void test1(struct block_header* bh) {
        printf("Test1\n");
        debug_heap(stderr, bh);
        printf("Обычное успешное выделение памяти\n");
        void* b1 = _malloc(128);
        debug_heap(stderr, bh);
        _free(b1);
        printf("\n");
}

void test2(struct block_header* bh) {
        printf("Test2\n");
        debug_heap(stderr, bh);
        printf("Освобождение одного блока из нескольких выделенных\n");
        void* b1 = _malloc(64);
        void* b2 = _malloc(128);
        debug_heap(stderr, bh);
        _free(b1);
        debug_heap(stderr, bh);
        _free(b2);
        printf("\n");
}

void test3(struct block_header* bh) {
        printf("Test3\n");
        debug_heap(stderr, bh);
        printf("Освобождение двух блоков из нескольких выделенных\n");
        void* b1 = _malloc(128);
        void* b2 = _malloc(64);
        void* b3 = _malloc(128);
        debug_heap(stderr, bh);
        _free(b2);
        _free(b3);
        debug_heap(stderr, bh);
        _free(b1);
        printf("\n");
}

void test4(struct block_header* bh) {
        printf("Test4\n");
        debug_heap(stderr, bh);
        printf("Память закончилась, новый регион памяти расширяет старый\n");
        void* b1 = _malloc(100000);
        void* b2 = _malloc(50000);
        debug_heap(stderr, bh);
        _free(b1);
        _free(b2);
        printf("\n");
}

void test5(struct block_header* bh) {
        printf("Test5\n");
        debug_heap(stderr, bh);
        printf("Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте\n");
        map_pages(find_last_block(bh), 5000, 0);
        void* b1 = _malloc(100000);
        void* b2 = _malloc(200000);
        debug_heap(stderr, bh);
        _free(b1);
        _free(b2);
        printf("\n");
}

int main() {
        struct block_header* bh = (struct block_header*) heap_init(100000);
        test1(bh);
        test2(bh);
        test3(bh);
        test4(bh);
        test5(bh);
}